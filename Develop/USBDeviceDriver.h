/**************************************************************
  * @file    USBDeviceDriver.h
  * @author
  * @created 
  * @revised 2019.07.25 S.L.
  * @brief
  *
  ************************************************************/
# ifndef _USBDEVICEDRIVER_H
# define _USBDEVICEDRIVER_H

# include "stm32f1xx_hal.h"
# include "cmsis_os.h"
# include "usbd_cdc_if.h"
//# include "protocol_tc200_self_test.h"
//# include "protocol_tc200_gimbal.h"
//# include "FIFOBuffer.h"

//# define TC200_SELF_TEST

//  typedef struct TxBuffCmplx
//  {
//    uint8_t bytes[32];
//    uint16_t length;
//  } TxBuffCmplxTypedef;
//  typedef FIFOBuffer<TxBuffCmplxTypedef, 64> TxBuffCmplxFifoTypedef;

  /*驱动*/
  class USBDriver
  {
 /*任务线程 - start*/
  public:
    static USBDriver* getInstance(void) {
      if (sInstance == NULL) sInstance = new USBDriver();
			return sInstance;
  }
//    void Run(void)
//    {
//      mTc200SelfTestPackHandler->run();
//      mTc200GimbalCtrlPackHandler->Run();
//      mTc200BoardStatusPackHandler->Run();
//      mTc200GimbalRttPackHandler->Run();
//      osThreadDef(USBDRIVERTASK, _Task, osPriorityAboveNormal, 0, 128);
//      osThreadCreate(osThread(USBDRIVERTASK), this);
//    }
    static USBDriver* sInstance; //唯一驱动实例
//  private:
//    USBDriver(void)
//    { 
//      asm("  NOP");
//    }
//    ~USBDriver() { ; }
//    osSemaphoreId mSemaphore;  //数据发送循环信号量
//    static void _Task(const void *);
//    static void _TimerCallBack(const void *); //定时器回调函数
//    osTimerId	_TimerId; //软件定时器
//    void _Loop(void)
//    {
//      osDelay(1000);
//      while(true)
//      {
//        //osSemaphoreWait(mSemaphore,osWaitForever);
//        osDelay(50);
//        _PushTc200GimbalCtrlUpstreamPackIntoXsmitFifo();
//        _PushTc200GimbalRttUpstreamPackIntoXsmitFifo();
//        _PushTc200BoardStatusUpstreamPackIntoXsmitFifo();
//        _PushTc200SelfTestUpstreamPackIntoXsmitFifo();
//      # if !RING_BUFF_TEST
//        if (_PopPackOutOfTxFifo())
//          CDC_Transmit_FS(mTxPackPopedOutHandler, mTxPackPopedOutLength);
//      # endif
//        mTc200SelfTestPackHandler->DownStreamCommandManagement();
//      }
//    }
// /*任务线程 - end*/

// /*数据发送和接收 - start*/
//  public:
//    void PushPackInToTxFifo(const void *bytes_handler, const void *pack_length)
//    {
//      _PushPackInToTxFifo(bytes_handler, pack_length);
//    }
//  private:
//    TxBuffCmplxFifoTypedef mTxBuffCmplxFifo;
//    TxBuffCmplxTypedef mTxBuffCmplxPopedOut;
//    void _PushPackInToTxFifo(const void *bytes_handler, const void *pack_length)
//    {
//      uint16_t length(*(uint16_t*)(pack_length));
//      if (length > 32) return;
//      uint8_t *bytes = (uint8_t*)bytes_handler;
//      TxBuffCmplxTypedef pack2push;
//      for (int i = 0; i < length; i++)
//      {
//        pack2push.bytes[i] = bytes[i];
//      }
//      pack2push.length = length;
//      if (!mTxBuffCmplxFifo.ResSize())
//      {
//        TxBuffCmplxTypedef pack2desert;
//        mTxBuffCmplxFifo.Get(pack2desert);
//      }
//      mTxBuffCmplxFifo.Put(pack2push);
//    }
//    uint8_t *mTxPackPopedOutHandler;
//    uint16_t mTxPackPopedOutLength;
//    bool _PopPackOutOfTxFifo(void)
//    {
//      if(!mTxBuffCmplxFifo.Size())
//        return false;
//      TxBuffCmplxTypedef pack_poped_out;
//      mTxBuffCmplxFifo.Get(pack_poped_out);
//      mTxPackPopedOutHandler = pack_poped_out.bytes;
//      mTxPackPopedOutLength = pack_poped_out.length;
//      return true;
//    }
//  private:
//    uint8_t mRxBuff[32];
//    uint32_t mRxLength;
//    bool _PushTc200GimbalCtrlUpstreamPackIntoXsmitFifo(void)
//    {
//    //mTc200GimbalCtrlPackHandler->ReleaseSem();
//      if(mTc200GimbalCtrlPackHandler->IfUpstreamPackNeed2Xsmit())
//      {
//        mTc200GimbalCtrlPackHandler->PrepareUpstreamPack();
//        uint8_t *pack_handler = mTc200GimbalCtrlPackHandler->UpstreamPackHandler;
//        uint16_t pack_length = mTc200GimbalCtrlPackHandler->PackLength;
//        _PushPackInToTxFifo(pack_handler, &pack_length);
//        return true;
//      }
//      else
//        return false;
//    }
//    bool _PushTc200GimbalRttUpstreamPackIntoXsmitFifo(void)
//    {
//    //mTc200GimbalRttPackHandler->ReleaseSem();
//      if(mTc200GimbalRttPackHandler->IfUpstreamPackIsReady())
//      {
//        mTc200GimbalRttPackHandler->PopPackOutOfUpstreamFifo();
//        uint8_t *pack_handler = mTc200GimbalRttPackHandler->UpstreamPackHandler;
//        uint16_t pack_length = mTc200GimbalRttPackHandler->PackLength;
//        _PushPackInToTxFifo(pack_handler, &pack_length);
//        return true;
//      }
//      else if(mTc200GimbalRttPackHandler->ifHeartBeatPackIsReady())
//      {
//        mTc200GimbalRttPackHandler->PopPackOutOfHeartBeatFifo();
//        uint8_t *pack_handler = mTc200GimbalRttPackHandler->HeartBeatPackHandler;
//        uint16_t pack_length = mTc200GimbalRttPackHandler->HeartBeatPackLength;
//        _PushPackInToTxFifo(pack_handler, &pack_length);
//        return true;
//      }
//      else
//        return false;
//    }
//    bool _PushTc200BoardStatusUpstreamPackIntoXsmitFifo(void)
//    {
//    //mTc200BoardStatusPackHandler->ReleaseSem();
//      if(mTc200BoardStatusPackHandler->IfUpstreamPackIsReady())
//      {
//        mTc200BoardStatusPackHandler->PopPackOutOfUpstreamFifo();
//        uint8_t *pack_handler = mTc200BoardStatusPackHandler->UpstreamPackHandler;
//        uint16_t pack_length = mTc200BoardStatusPackHandler->PackLength;
//        _PushPackInToTxFifo(pack_handler, &pack_length);
//        return true;
//      }
//      else
//        return false;
//    }
//    bool _PushTc200SelfTestUpstreamPackIntoXsmitFifo(void)
//    {
//    /*transmit upstream pack - start*/
//      mTc200SelfTestPackHandler->ReleaseSem();
//      if (mTc200SelfTestPackHandler->IfUpstreamPackIsReady())
//      {
//        mTc200SelfTestPackHandler->PopPackOutOfUpstreamFifo();
//        uint8_t *pack_handler = mTc200SelfTestPackHandler->UpstreamPackHandler;
//        uint16_t pack_length = mTc200SelfTestPackHandler->PackLength;
//        _PushPackInToTxFifo(pack_handler, &pack_length);
//        return true;
//      }
//      else
//        return false;
//    /*transmit upstream pack - end*/
//    }
//    void _ReceiveTc200SelfTestDownstreamPack(uint8_t *buff_src, uint32_t num)
//    {
//      uint8_t lngth = (num>32)? 32 : num;
//      memcpy(mRxBuff, buff_src, lngth);
//      mRxLength = lngth;
//      if (mTc200SelfTestPackHandler->DownstreamPackCheckout(mRxBuff))
//      {
//        mTc200SelfTestPackHandler->PushPackInDownstreamFifo(mRxBuff);
//        mTc200SelfTestPackHandler->ReturnDonwstreamPack2Host();
//      }
//      if (mTc200BoardStatusPackHandler->DownstreamPackCheckout(mRxBuff))
//      {
//        mTc200BoardStatusPackHandler->CmdManager();
//        mTc200BoardStatusPackHandler->ReturnDonwstreamPack2Host();
//      }
//    }
//    //static ProtocalTc200SelfTestTypedef sTc200SelfTestDownstreamPack;
  public:
    void ReceiveIsrHandler(uint8_t *buff_src, uint32_t num)
    {
      //_ReceiveTc200SelfTestDownstreamPack(buff_src,num);
    }
 /*数据发送和接收 - end*/

// /*协议对象句柄 - start*/
//  private:
//    Tc200SelfTestPackManagment *mTc200SelfTestPackHandler;
//    Tc200GimbalCtrlPackManagment *mTc200GimbalCtrlPackHandler;
//    Tc200BoardStatusPackManagment *mTc200BoardStatusPackHandler;
//    Tc200GimbalRttPackManagment *mTc200GimbalRttPackHandler;
// /*协议对象句柄 - end*/
  };
    
# endif /*# ifndef _USBDEVICEDRIVER_H*/
//no more.
//
//
//

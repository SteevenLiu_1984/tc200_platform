/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.cpp
  * @brief          : Main program body
  * @revised        : 2019.07.01 by Steven L.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"
#include "adc.h"
#include "dma.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"
/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

# if 0 // APP
  #include "define.h"
  #include "key_scan_task.h"
  #include "rf_com.h"
  #include "env.h"
  
  #include "tone.h"
  #include "usbd_cdc_if.h"
  #include "nRF24L01_106V.h"
  #include "USBDeviceDriver.h"
  #include "led_driver.h"
  #include "RemoteIdConfig.h"
  # include "ring_lib.h"
# endif
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

  # include <map>
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
 # define CLK_Buzz_TIM 72000000
 # define ARR_Buzz_TIM 100

 # define BUZZ_VOL_VERYLOW      htim2.Instance->ARR/32
 # define BUZZ_VOL_LOW          htim2.Instance->ARR/16
 # define BUZZ_VOL_MEDIUM       htim2.Instance->ARR/8
 # define BUZZ_VOL_ABOVEMEDIUM  htim2.Instance->ARR/4
 # define BUZZ_VOL_HIGH         htim2.Instance->ARR/2
 # define BUZZ_VOL_VERYHIGH     htim2.Instance->ARR*15/16

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
# include "USBDeviceDriver.h"
  USBDriver* USBDriver::sInstance = NULL;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
# ifdef __cplusplus
  extern "C"
  {
# endif /*__cplusplus*/
    void SystemClock_Config(void);
    void MX_FREERTOS_Init(void);
# ifdef __cplusplus
  }
# endif /*__cplusplus*/

/* USER CODE BEGIN PFP */
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

# if 0 //APP
 osSemaphoreId sem_js_started = NULL;
 RfComDriver* rf_com = NULL;

 void task_joystick_startup(const void *arg)
 {
   while (g_buzzer_handler == NULL) { ; }
   while (sem_js_started == NULL) { ; }
   //g_buzzer_handler->buzz_test_bench();
   //g_buzzer_handler->buzz(Buzz_Mi_Re_Do);
   //g_buzzer_handler->buzz(Buzz_Beeeep);

   /* 等待3秒 嘟 嘟 嘟 */
   uint16_t tick = 0;
   while (tick++ < 3)
   {
     g_buzzer_handler->buzz(Buzz_Beeeep);
	 //g_buzzer_handler->buzz(Buzz_Mi_Re_Do);
     osDelay(1000);
   }
  // /* 开始校准 等待1秒 嘟-- */
   g_buzzer_handler->set_tone(TonePure::Hz440.La);
   g_buzzer_handler->set_volume(buzz_vol_very_low(htim2.Instance));
   g_buzzer_handler->buzz(Buzz_On);
   osDelay(1000);
   g_buzzer_handler->terminate_buzz();
  // /* 校准完成 嘟嘟嘟 */
   osDelay(500);
   g_buzzer_handler->buzz(Buzz_Mi_Re_Do);
   osSemaphoreRelease(sem_js_started);
   osThreadTerminate(osThreadGetId());
 }


 void default_task(const void *arg)
 {
   osDelay(100);
# if 1
/* 初始化环境变量 */
   int env1_status = init_env();

/*  */
   g_rf_link_status.rfused = false;

/* 创建全局信号量 */
   creat_sem(); //创建全局信号量

/* ... */
   creat_timer(); //创建全局软件定时器

/* ... */
   start_config_task();
# endif

/* 蜂鸣器 - start */
   TonePure::getInstance(CLK_Buzz_TIM,ARR_Buzz_TIM);
   htim2.Instance->ARR = ARR_Buzz_TIM;
   g_buzzer_handler = Buzzer::getInstance(&htim2, TIM_CHANNEL_4, buzz_vol_very_low(htim2.Instance));// BUZZ_VOL_VERYLOW/*BUZZ_VOL_LOW*/);
   //g_buzzer_handler->buzz_test_bench();
/* 蜂鸣器 - end */

# if 1
/* LED灯 - start */
   g_led_m1.setGpio(L1_M1_GPIO_Port, L1_M1_Pin);
   g_led_m2.setGpio(L2_M2_GPIO_Port, L2_M2_Pin);
   init_led_m1();
   init_led_m2();
   g_led_power.setGpio(L3_ON_OFF_GPIO_Port, L3_ON_OFF_Pin);
   g_led_power_green.setGpio(L4_ON_OFF_2_GPIO_Port, L4_ON_OFF_2_Pin);
   g_led_power_red.setGpio(L4_ON_OFF_1_GPIO_Port, L4_ON_OFF_1_Pin);
   init_led_power();
   init_led_power_green();
   init_led_power_red();
/* LED灯 - end */

/* ADC扫描器初始化 - start */
   g_adc_scaner_handler = AdcScaner::getInstance();
   g_adc_scaner_handler->init(&g_self_param);
   g_adc_scaner_handler->run();
/* ADC扫描器初始化 - end */

   //g_buzzer_handler->buzz_test_bench();
/* 摇杆校准 - start */
   osSemaphoreDef(SMF_JOYSTICK_STARTED);
   sem_js_started = osSemaphoreCreate(osSemaphore(SMF_JOYSTICK_STARTED), 1);
   osSemaphoreWait(sem_js_started, 0);

   //osThreadDef(TSK_JS_STARTUP, task_joystick_startup, osPriorityNormal, 1, 128);
   //osThreadCreate(osThread(TSK_JS_STARTUP), (void *)0);

   while (osSemaphoreWait(sem_js_started, osWaitForever) != osOK) { ; } //等候校准完成
/* 摇杆校准 - end */

/* 按键扫描 - satart */
   creat_key_task();
   osDelay(5000);
   g_buzzer_handler->release_key_block();
/* 按键扫描 - end */


 # if 0
   uint32_t tick = 1000;
    
   while(tick--)
   {
     uint8_t buffer[] = "Hello World~";
     CDC_Transmit_FS(buffer,sizeof(buffer));
     osDelay(500);
   }
 # endif //测试USB虚拟串口

/* 遥控器数传天线 - start */
   g_rf24l01_handler = nRF24L01:: getInstance ( &hspi3
                                              , Lora_CE_GPIO_Port, Lora_CE_Pin
                                              , Lora_CS_GPIO_Port, Lora_CS_Pin );
   RFConnect::setLed1Handler(&g_led_m1);
   RFConnect::setLed2Handler(&g_led_m2);
/* 遥控器数传天线 - end */

/* USB虚拟串口驱动 - start */
   USBDriver::getInstance();
   USBDriver::sInstance-> Run();
/* USB虚拟串口驱动 - end */

  /*...*/
 # if RING_BUFF_TEST /*使用环缓冲记录和打印摇杆动态数据*/
   g_ring_buff_handler = RingBuffDriver::getInstance();
   g_ring_buff_handler->Init(CDC_Transmit_FS);
   if (!g_ring_buff_handler->RingCreate(100, typeid(uint8_t).name(), RING_0) ||
       !g_ring_buff_handler->RingCreate(100, typeid(uint8_t).name(), RING_2) ||
       !g_ring_buff_handler->RingCreate(100, typeid(uint8_t).name(), RING_3) ||
       !g_ring_buff_handler->RingCreate(100, typeid(uint8_t).name(), RING_4) ||
       !g_ring_buff_handler->RingCreate(100, typeid(uint8_t).name(), RING_5))
   {
     while (true)
     {
       asm("  NOP");
     }
   }
   g_ring_buff_handler->Run();
 # endif

   rf_com = RfComDriver::getInstance();
   if (rf_com->Rfcom_Init(env1_status, g_rf24l01_handler))//, g_adc_scaner_handler))
   {
     rf_com->Run();
     start_rf_timer();
   }

/* 风扇 - start */
   g_fun_handler = FunDriver::getInstance();
   g_fun_handler->init_Fun1( &htim3, TIM_CHANNEL_4
                         //, &g_adc_scaner_handler->temp_Case.t2
                           , &g_adc_scaner_handler->temp_Case.tmax
                           , &g_if_fun_enabled );
   g_fun_handler->init_Fun2( &htim1, TIM_CHANNEL_2
                         //, &g_adc_scaner_handler->temp_Case.t1
                           , &g_adc_scaner_handler->temp_Case.tmax
                           , &g_if_fun_enabled );
   g_fun_handler->run();
/* 风扇 - end */

/* 重启USB-HUB - start */
   osDelay(3000);
   HAL_GPIO_WritePin(USB_HUB_Reset__GPIO_Port, USB_HUB_Reset__Pin, GPIO_PIN_RESET);
   osDelay(500);
   HAL_GPIO_WritePin(USB_HUB_Reset__GPIO_Port, USB_HUB_Reset__Pin, GPIO_PIN_SET);
/* 重启USB-HUB - end */
# endif

	 //std::map<>
	 
/* 线程挂起 */
   osThreadSuspend(osThreadGetId());

/* 不会运行到这里 */
   while (true) { ; }
 }


#endif

 /* USER CODE END 0 */

//  uint16_t tar_DMA[6];

# define VECTOR_SIZE 0x130
# define RAM_ADDR_VCTABLE 0x20000000
# define FLASH_ADDR_VCTABLE 0x08010000

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  memcpy((void*)RAM_ADDR_VCTABLE,(void*)FLASH_ADDR_VCTABLE,VECTOR_SIZE);
  SCB->VTOR = RAM_ADDR_VCTABLE;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_I2C1_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM6_Init();
  MX_UART4_Init();
  MX_SPI3_Init();
  MX_ADC1_Init();

  /* USER CODE BEGIN 2 */

  /*启动网卡*/
  //HAL_GPIO_WritePin(Reset__Ether1_GPIO_Port, Reset__Ether1_Pin, GPIO_PIN_SET);
  //HAL_GPIO_WritePin(Reset__Ether2_GPIO_Port, Reset__Ether2_Pin, GPIO_PIN_SET);
  //HAL_GPIO_WritePin(DIS__EtherPower_GPIO_Port, DIS__EtherPower_Pin, GPIO_PIN_SET);

  /*启动风扇电源*/
  //HAL_GPIO_WritePin(DIS__FUN_5V_BAT_GPIO_Port, DIS__FUN_5V_BAT_Pin, GPIO_PIN_SET);

  /* USER CODE END 2 */

  /* Call init function for freertos objects (in freertos.c) */
  MX_FREERTOS_Init();

  //printf("\r\n");
  //printf("JOUAV TC200 Build-in Software. \r\n");
  //printf("Build date: "); printf(__DATE__); printf("\r\n");
  //printf("Build time: "); printf(__TIME__); printf("\r\n");
  //printf("Author: "); printf("Steven.L."); printf("\r\n");
  //printf("Parameters:\r\n");
# if IF_POWER_LED_GREEN_AND_RED
  //printf("  1. Power LED: double.\r\n");
# else
  printf("  1. Power LED: single.\r\n");
# endif
  /* Start scheduler */
  osKernelStart();
  
  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_ADC|RCC_PERIPHCLK_USB;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_PLL_DIV1_5;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

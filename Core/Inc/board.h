/**************************************************************
  * @file    board.h
  * @author  Steven L.
  * @C.D.    2019.03.27
  * @brief
  *
  ************************************************************/

# ifndef _BOARD_H
# define _BOARD_H
# include "stm32f1xx.h"

# define Message_CanTxDataTest {'R', 'C', 'V', 'R', ' ', '-', ' ', 'C', 'A', 'N', ' ', ' ', ' ', ' ', 0x0D, 0x0A} //测试CAN通信用 

# define NRF24L01_PAIR_TEST 1         // 配对测试：1 - 使能；0 - 禁止
# define NRF24L_Physical_ID 0x78,0x78,0x78,0x78,0x78 // PTX/PRX模式地址（物理ID）
# define NRF24L_Working_MODE MODE_RX  // 接收机工作模式：MODE_RX - 接收(PRX模式)；MODE_TX - 发送(PTX模式)
# define NRF24L_Trans_MODE 0          // 接收机PTX发送方式：1 连续发送；0 单次发送

//# define nRF24L01_CE_GPIO_PORT GPIOA 
//# define nRF24L01_CE_GPIO_PIN GPIO_PIN_3

# define MMP_FLASH_ADDR_STM32F03xx 0x08007000
# define MMP_FLASH_ADDR_STM32F04xx 0x08007000
# define MMP_FLASH_ADDR_STM32F05xx 0x0800F000
# define MMP_FLASH_ADDR_STM32F07xx 0x0801F000
# define MMP_FLASH_ADDR_STM32F09xx 0x0803F000

# endif // "#ifndef _BOARD_H"

//no more.

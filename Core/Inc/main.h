/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under Ultimate Liberty license
  * SLA0044, the "License"; You may not use this file except in compliance with
  * the License. You may obtain a copy of the License at:
  *                             www.st.com/SLA0044
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define Reset__Ether2_Pin GPIO_PIN_2
#define Reset__Ether2_GPIO_Port GPIOE
#define T2_FL_Pin GPIO_PIN_3
#define T2_FL_GPIO_Port GPIOE
#define T3_Landing_Pin GPIO_PIN_4
#define T3_Landing_GPIO_Port GPIOE
#define L1_M1_Pin GPIO_PIN_5
#define L1_M1_GPIO_Port GPIOE
#define L2_M2_Pin GPIO_PIN_6
#define L2_M2_GPIO_Port GPIOE
#define H2_Gimbal_Pin GPIO_PIN_14
#define H2_Gimbal_GPIO_Port GPIOC
#define H2_RC_Pin GPIO_PIN_13
#define H2_RC_GPIO_Port GPIOC
#define Reset__Ether1_Pin GPIO_PIN_15
#define Reset__Ether1_GPIO_Port GPIOC
#define H3_Y_Pin GPIO_PIN_0
#define H3_Y_GPIO_Port GPIOC
#define H3_X_Pin GPIO_PIN_1
#define H3_X_GPIO_Port GPIOC
#define H3_Key_Pin GPIO_PIN_2
#define H3_Key_GPIO_Port GPIOC
#define T4_Engine_Start_Pin GPIO_PIN_3
#define T4_Engine_Start_GPIO_Port GPIOC
#define T5_Engine_Stop_Pin GPIO_PIN_0
#define T5_Engine_Stop_GPIO_Port GPIOA
#define T6_Prop_Adj_Pin GPIO_PIN_1
#define T6_Prop_Adj_GPIO_Port GPIOA
#define L3_ON_OFF_Pin GPIO_PIN_2
#define L3_ON_OFF_GPIO_Port GPIOA
#define L4_ON_OFF_1_Pin GPIO_PIN_8
#define L4_ON_OFF_1_GPIO_Port GPIOE
#define L4_ON_OFF_2_Pin GPIO_PIN_9
#define L4_ON_OFF_2_GPIO_Port GPIOE
#define H5_Buzz_PWM_Pin GPIO_PIN_3
#define H5_Buzz_PWM_GPIO_Port GPIOA
#define Temp1_Pin GPIO_PIN_4
#define Temp1_GPIO_Port GPIOA
#define Temp2_Pin GPIO_PIN_5
#define Temp2_GPIO_Port GPIOA
#define VoltBat_Rp_Pin GPIO_PIN_7
#define VoltBat_Rp_GPIO_Port GPIOA
#define VoltBat_Ln_Pin GPIO_PIN_6
#define VoltBat_Ln_GPIO_Port GPIOA
#define Temp1_GPIO_Port GPIOA
#define H1_Key_Pin GPIO_PIN_5
#define H1_Key_GPIO_Port GPIOC
#define H1_X_Pin GPIO_PIN_0
#define H1_X_GPIO_Port GPIOB
#define H1_Y_Pin GPIO_PIN_1
#define H1_Y_GPIO_Port GPIOB
#define G18_Cr__Pin GPIO_PIN_10
#define G18_Cr__GPIO_Port GPIOE
#define G17_Cr__Pin GPIO_PIN_11
#define G17_Cr__GPIO_Port GPIOE
#define G16_B__Pin GPIO_PIN_12
#define G16_B__GPIO_Port GPIOE
#define G15_B__Pin GPIO_PIN_13
#define G15_B__GPIO_Port GPIOE
#define G14_FOV__Pin GPIO_PIN_14
#define G14_FOV__GPIO_Port GPIOE
#define G13_FOV__Pin GPIO_PIN_15
#define G13_FOV__GPIO_Port GPIOE
#define G12_OSD_Pin GPIO_PIN_10
#define G12_OSD_GPIO_Port GPIOB
#define G11_LOCK_Pin GPIO_PIN_11
#define G11_LOCK_GPIO_Port GPIOB
#define G10_FL_Pin GPIO_PIN_12
#define G10_FL_GPIO_Port GPIOB
#define G9_MT_AT_Pin GPIO_PIN_13
#define G9_MT_AT_GPIO_Port GPIOB
#define G8_Pos_Renew_Pin GPIO_PIN_14
#define G8_Pos_Renew_GPIO_Port GPIOB
#define G7_RTT_M_Pin GPIO_PIN_15
#define G7_RTT_M_GPIO_Port GPIOB
#define G6_RTT_A_Pin GPIO_PIN_8
#define G6_RTT_A_GPIO_Port GPIOD
#define G5_NUC_Pin GPIO_PIN_9
#define G5_NUC_GPIO_Port GPIOD
#define G4_ES_Pin GPIO_PIN_10
#define G4_ES_GPIO_Port GPIOD
#define G3_VM_Pin GPIO_PIN_11
#define G3_VM_GPIO_Port GPIOD
#define G2_TV_IR_Pin GPIO_PIN_12
#define G2_TV_IR_GPIO_Port GPIOD
#define T1_Manual_Pin GPIO_PIN_15
#define T1_Manual_GPIO_Port GPIOD
#define T1_Semi_Auto_Pin GPIO_PIN_14
#define T1_Semi_Auto_GPIO_Port GPIOD
#define T1_Auto_Pin GPIO_PIN_13
#define T1_Auto_GPIO_Port GPIOD
#define G1_SnapShot_Pin GPIO_PIN_6
#define G1_SnapShot_GPIO_Port GPIOC
#define USB_HUB_Reset__Pin GPIO_PIN_15
#define USB_HUB_Reset__GPIO_Port GPIOA
#define PD0_Pin GPIO_PIN_0
#define PD0_GPIO_Port GPIOD
#define PD1_Pin GPIO_PIN_1
#define PD1_GPIO_Port GPIOD
#define PD2_Pin GPIO_PIN_2
#define PD2_GPIO_Port GPIOD
#define PD3_Pin GPIO_PIN_3
#define PD3_GPIO_Port GPIOD
#define GPIO_EXTI7_Pin GPIO_PIN_7
#define GPIO_EXTI7_GPIO_Port GPIOD
#define GPIO_EXTI7_EXTI_IRQn EXTI9_5_IRQn
#define SPI3_CS__Pin GPIO_PIN_6
#define SPI3_CS__GPIO_Port GPIOB
#define GPIO_OUT_PB7_Pin GPIO_PIN_7
#define GPIO_OUT_PB7_GPIO_Port GPIOB
#define DIS__FUN_5V_BAT_Pin GPIO_PIN_0
#define DIS__FUN_5V_BAT_GPIO_Port GPIOE
#define DIS__EtherPower_Pin GPIO_PIN_1
#define DIS__EtherPower_GPIO_Port GPIOE
#define USB_UP_Pin GPIO_PIN_7
#define USB_UP_GPIO_Port GPIOC

/*原TC101 EC11引脚分配到MCU空闲的三个引脚上*/
#define EC11_S2_Pin PD0_Pin
#define EC11_S2_GPIO_Port PD0_GPIO_Port
#define EC11_S1_Pin PD1_Pin
#define EC11_S1_GPIO_Port PD1_GPIO_Port
#define EC11_SW_Pin PD2_Pin
#define EC11_SW_GPIO_Port PD2_GPIO_Port

#define Lora_IRQ_Pin GPIO_EXTI7_Pin
#define Lora_IRQ_GPIO_Port GPIO_EXTI7_GPIO_Port
#define Lora_IRQ_EXTI_IRQn EXTI9_5_IRQn

#define Lora_CS_Pin SPI3_CS__Pin
#define Lora_CS_GPIO_Port SPI3_CS__GPIO_Port

#define Lora_CE_Pin GPIO_OUT_PB7_Pin
#define Lora_CE_GPIO_Port GPIO_OUT_PB7_GPIO_Port

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

#include <stdint.h>
#include <stdlib.h>
#include "cmsis_os.h"

void *malloc(size_t size)
{
	return pvPortMalloc(size);
}

void free(void *p)
{
	vPortFree(p);
}

#include "stdio.h"
#include "usart.h"

int fputc(int ch, FILE *f)
{
	uint8_t var = ch;
	HAL_UART_Transmit(&huart4, &var, 1, 1);
	while (! __HAL_UART_GET_FLAG(&huart4, UART_FLAG_TXE))
		__NOP();
	return ch;
}

